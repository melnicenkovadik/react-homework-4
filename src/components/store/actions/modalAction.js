export const TOGGLE_MODAL = "TOGGLE_MODAL";

const toggleModal = () => {
  return {type: TOGGLE_MODAL}
};
export default toggleModal