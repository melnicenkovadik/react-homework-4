import axios from 'axios'

export const FETCH_PRODUCTS = "FETCH_PRODUCTS";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const SET_PRODUCT_TO_CART = "SET_PRODUCT_TO_CART";
export const SET_PRODUCTS_TO_FAVORITES = "SET_PRODUCTS_TO_FAVORITES";

export const fetchProducts = () => dispatch => {
  if (!localStorage.getItem('cart') || !localStorage.getItem('chosen')) {
    localStorage.setItem('cart', JSON.stringify([]));
    localStorage.setItem('chosen', JSON.stringify([]));
  }
  dispatch({type: FETCH_PRODUCTS})
  axios.get('products.json')
    .then(response => {

      const localFav = JSON.parse(localStorage.getItem('chosen'));
      const localCart = JSON.parse(localStorage.getItem('cart'))
      const productsWithLocal = response.data.map(prod => {
        return {
          ...prod,
          isInCart: localCart.some(id => id === prod.id),
          isFav: localFav.some(id => id === prod.id),
        }
      })
      dispatch({type: FETCH_PRODUCTS_SUCCESS, payload: productsWithLocal})
    });
};

export const setProductToCart = id => dispatch => {
  const localCart = JSON.parse(localStorage.getItem('cart'));
  localCart.push(id)
  localStorage.setItem('cart', JSON.stringify(localCart))
  dispatch ({type: SET_PRODUCT_TO_CART, payload: id})

};

export const setProductToFavorites = id => dispatch => {
  const localFav = JSON.parse(localStorage.getItem('chosen'));
  localFav.push(id)
  localStorage.setItem('chosen', JSON.stringify(localFav))
  dispatch ({ type: SET_PRODUCTS_TO_FAVORITES,  payload: id})
};



