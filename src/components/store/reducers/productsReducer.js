import {
  FETCH_PRODUCTS,
  FETCH_PRODUCTS_SUCCESS,
  SET_PRODUCT_TO_CART,
  SET_PRODUCTS_TO_FAVORITES
} from '../actions/productsActions'

const initialState = {
  products: [],
  isFetching: false

}
const productsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PRODUCTS :
      return {
        ...state,
        isFetching: true
      }
    case FETCH_PRODUCTS_SUCCESS :
      return {
        ...state,
        isFetching: false,
        products: action.payload
      };
    case SET_PRODUCT_TO_CART:
      return {
        ...state,
        products: state.products.map(prod => {
          if (prod.id === action.payload) {
            return {
              ...prod,
              isInCart: !prod.isInCart
            }
          }else {
            return prod;
          }
        })
      };
    case SET_PRODUCTS_TO_FAVORITES :
      return {
        ...state,
        products: state.products.map(prod => {
          if (prod.id === action.payload) {
            return {
              ...prod,
              isFav: !prod.isFav
            }
          } else {
            return prod;
          }
        })
      }


    default:
      return state;
  }

}
export default productsReducer