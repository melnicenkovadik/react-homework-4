import {combineReducers} from "redux";
import productsReducer from "./productsReducer";
import modalReducer from "./modalReducer";

const rootReducer = combineReducers({
  products:productsReducer,
  modal: modalReducer
});

export default rootReducer