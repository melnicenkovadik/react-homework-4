import {TOGGLE_MODAL} from "../actions/modalAction";

const initialState = {
  modal: false

}
const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_MODAL :
      return {
        ...state,
        modal: !state.modal
      }
    default:
      return state;
  }
}
export default modalReducer
