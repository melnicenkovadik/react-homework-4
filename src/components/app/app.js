import React, {useEffect, useState} from 'react';
import MyModal from '../my-modal'
import './app.css';
import {HomePage} from '../pages';
import {BrowserRouter as Router, Route, Switch,} from "react-router-dom";
import {connect} from "react-redux";
import {fetchProducts, setProductToCart, setProductToFavorites} from '../store/actions/productsActions'
import toggleModal from '../store/actions/modalAction'


const filteredData = (allData, idsArray) => {
  return allData.filter((item) => {
    if (idsArray.includes(item.id)) {
      return item;
    }
  })
};
const App = props => {
  // const [data, setDate] = useState(null);
  // const [modal, setModal] = useState(false);
  const [modalActoin, setModalAction] = useState(null);
  const [cartItem, setCartItem] = useState(null);


  useEffect(() => {
    props.fetchProducts()
  }, []);


  let onAddToCartConfirm = () => {
    if (cartItem) {
      const {id} = cartItem;
      // writeToLocalStorage("cart", id);
      // onCloseModal();
    }
  };

  let onDeleteFromCartConfirm = () => {
    if (cartItem) {
      const {id} = cartItem;
      // deleteFromLocalStorage("cart", id);
      // onCloseModal();
    }
  };

  let onAddToCart = (item) => {
    setCartItem(item);
    // setModal(true);
    setModalAction('add');
    props.toggleModal();
  };

  let onDeleteFromCart = (item) => {
    setCartItem(item);
    // setModal(true);
    setModalAction('delete');
  };

  let switchModalActions = (action) => {
    switch (action) {
      case 'add':
        return {
          title: "Додавання в корзину",
          question: "Ви впевнені що хочете додати товар в корзину?",
          confirmTitle: "Додати в корзину",
          action: onAddToCartConfirm
        }
      case 'delete':
        return {
          title: "Видалення з корзини",
          question: "Ви впевнені що хочете видалити товар з корзини?",
          confirmTitle: "Видалити з корзини",
          action: onDeleteFromCartConfirm
        }
      default:
        return {title: "", question: "", action: props.toggleModal}
    }
  }

  const {title, question, confirmTitle, action} = switchModalActions(modalActoin);

  return (
    <div className="App">
      <Router>
        <MyModal
          title={title}
          question={question}
          showModal={props.modal}
          action={action}
          confirmTitle={confirmTitle}
          onCloseModal={props.toggleModal}
        />

        <Switch>
          <Route path="/cart">
            <CartPage data={}
                      onDeleteFromCart={onDeleteFromCart}
                      onChooseItem={toogleChooseItem}/>
          </Route>
          <Route path="/chosen">
            <ChosenPage data={filteredData(data, getDataFromLocalStorage('chosen'))}
                        onAddToCart={}
                        onChooseItem={toogleChooseItem}/>
          </Route>
          <Route path="/">
            <HomePage data={props.products} onAddToCart={onAddToCart} onChooseItem={() => {
            }}/>
          </Route>
        </Switch>
      </Router>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    products: state.products.products,
    modal: state.modal.modal,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProducts: () => dispatch(fetchProducts()),
    toggleModal: () => dispatch(toggleModal()),
    setProductToCart: (id) => dispatch(setProductToCart(id)),
    setProductToFavorites: (id) => dispatch(setProductToFavorites(id))

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(App)
