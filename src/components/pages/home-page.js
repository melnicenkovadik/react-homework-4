import React from 'react';
import Header from '../header';
import ProductList from '../product-list';

const HomePage = ({ data, onAddToCart, onChooseItem}) => {
    return (
        <React.Fragment>
            <Header />
            <ProductList data={data} onAddToCart={onAddToCart } onChooseItem={onChooseItem }/>
        </React.Fragment>
    )
}

export default HomePage;