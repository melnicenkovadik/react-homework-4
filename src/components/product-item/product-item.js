import React from 'react'
import './product-item.css';

const ProductItem = (props) => {
  console.log(props)
  const {data, onAddToCart, onChooseItem, onDeleteFromCart, displayAddButton, displayDeleteButton} = props;
  const chosen = (data.isFav) ? " chosen" : "";
  return (
    <div className="card cart-card product-item m-3 p-3 " style={{width: '18rem'}}>
      <div><h5 className="card-title float-left">{data.name}</h5>
        <h6 className="float-right card-text">Артикул:{data.article}</h6>
      </div>
      <hr/>
      <div className='card-img-top '>
        <img className="card-img-top product-item-img" src={data.imgUrl} alt="lol"></img>
      </div>
      <div className="card-body">
        <p className="card-text">Ціна:{data.price}</p>
        <p className="card-text">Колір:{data.color}</p>
        <hr/>
        <div className="d-flex justify-content-between buttons">
          {displayAddButton && <button className='btn btn-primary  ' onClick={onAddToCart}>Додати в корзину</button>}
          {displayDeleteButton && <button onClick={onDeleteFromCart}>X</button>}
          <div className={` product-item-chosen-button ${chosen}`} onClick={onChooseItem}>
            <i className="las la-star"></i>
          </div>
        </div>
      </div>
    </div>
  )

}

export default ProductItem